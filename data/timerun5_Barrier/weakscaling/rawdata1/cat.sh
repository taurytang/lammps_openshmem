

for i in 4096 512 64 8
do
  cat timeMPI.txt | grep " ${i} " >> mpi${i}.txt
  cat timeSHMEM.txt | grep " ${i} " >> shmem${i}.txt
done
