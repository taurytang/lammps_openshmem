# Plotting the data of file with points and continuous lines
 set term postscript eps
 set output "remap3d_strscal_Barrier.eps"
  
 set size square .66

########################
#define axis
set style line 2 lc rgb '#000000' lt 1 lw 2   # ---black
set border  back ls 2

# set color and size of points and lines, and pointslinesinterval
set style line 1 lc rgb 'red' lt 1 lw 2 pt 3  #----red
set style line 2 lc rgb '#00c000' lt 2 lw 2 pt 8 #----web-green
#set style line 3 lc rgb 'blue' lt 2 lw 2 pt 4 

set style line 3 lc rgb "#000000" lt 2 lw 2
set style line 4 lc rgb "#0060ad" lt 1 lw 2 pt 1
set style line 5 lc rgb "#8a2be2" lt 1 lw 2 pt 2
set style line 6 lc rgb "#000000" lt 1 lw 2

#set style line 1 lc rgb '#ff0000' lt 1 lw 1.5 pt 7 pi -1 ps 1.5 #----red
#set pointintervalbox 2

# define grid
set style line 12 lc rgb '#7f7f7f' lt 0 lw 1 # -----gray
set grid back ls 12

#########################

set xlabel "Number of Cores"
set ylabel "Time Elapsed (sec)"

#set title "Performance of remap_3d"
set key top right  

#set logscal_Barrier x
set xtics ( "256" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
set ytics 1
set yr [0:8]
set xr [0.5:5.5]

plot "shmemAvg.txt" using 1:10:($12) title "Remap_3d_oSHMEM" with yerrorbars ls 4,\
     "" using 1:10 title "" with lines ls 4,\
     "mpiAvg.txt" using 1:10:($12) title "Remap_3d_MPI" with yerrorbars ls 5,\
     "" using 1:10:12 title "" with lines ls 5


!epstopdf remap3d_strscal_Barrier.eps && rm remap3d_strscal_Barrier.eps


