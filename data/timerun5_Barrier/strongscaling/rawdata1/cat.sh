

for i in 4096 2048 1024 512 256
do
  cat timeMPI.txt | grep " ${i} " >> mpi${i}.txt
  cat timeSHMEM.txt | grep " ${i} " >> shmem${i}.txt
done
