#include "globals.h"
#include "mpp/shmem.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"



// main.cpp -------------------
int pWrk1[_SHMEM_REDUCE_MIN_WRKDATA_SIZE];
long pSync1[_SHMEM_REDUCE_SYNC_SIZE];
double pWrk2[_SHMEM_REDUCE_MIN_WRKDATA_SIZE];

//------------------------------
//input.h
int input_nfile;
long input_nline;
char input_array[800][200];
char *input_line;
long pSync[_SHMEM_BCAST_SYNC_SIZE];

//globals
int gi, gj, gk, gstep;
double gtimeLammps;
double gtimeRemap, gtimeFFT3d, gtimePack, gtimeUnpack, gtimeIntP;

MyTime *gtimePPPM;
MyTime *gtimeMid1;
MyTime *gtimeMid2;
MyTime *gtimePost;

MyFFT *gtimeFFT;
//-----------------------------
//remap.cpp
int remap_temp;

//pppm.h
int pppm_temp;
