#ifndef LMP_GLOBALS_H
#define LMP_GLOBALS_H

#include "mpi.h"
#include "mpp/shmem.h"
#include "stdio.h"


#ifdef FFT_SINGLE
typedef float FFT_SCALAR;
#define MPI_FFT_SCALAR MPI_FLOAT
#else
typedef double FFT_SCALAR;
#define MPI_FFT_SCALAR MPI_DOUBLE
#endif


//main.cpp
extern int pWrk1[_SHMEM_REDUCE_MIN_WRKDATA_SIZE];
extern long pSync1[_SHMEM_REDUCE_SYNC_SIZE];
extern double pWrk2[_SHMEM_REDUCE_MIN_WRKDATA_SIZE];


//input.h
extern long input_nline;
extern char input_array[800][200];
extern char *input_line;
extern int input_nfile;
extern long pSync[_SHMEM_BCAST_SYNC_SIZE];

//globals
extern int gi, gj, gk, gstep;
extern double gtimeLammps, gtimeFFT3d;
extern double gtimeRemap, gtimePack, gtimeUnpack, gtimeIntP;

class MyTime{
  public:
   double remap;
   double pack;
   double unpack;
   double barrier;
   double IntP;
   int rank;
   int sendsize;
   int recvsize;
};
extern MyTime *gtimePPPM;
extern MyTime *gtimeMid1;
extern MyTime *gtimeMid2;
extern MyTime *gtimePost;


class MyFFT{
  public:
    double fast;
    double mid;
    double slow;
};
extern MyFFT *gtimeFFT;

//-------------------------------
//remap.cpp
extern int remap_temp;

//pppm.h
extern int pppm_temp;

#endif
