Porting openSHMEM to LAMMPS and compare the performance.
By Chunyan Tang  since 2013

# versions of the code
MPI version: Original code of lammps_28Jun2014.
oshmem_v0: Use MPI_Barrier in Remap_3d() for synchronization
oshmem_v1: Use one MPI_Barrier for each FFT3D
oshmem_v2: Remove the Barrier. Check the peer's buffer status to avoid deadlock.
           Pack and send data to all the peers first, and then unpack the received data.
oshmem_v3: Remove the Barrier. Check the peer's buffer status to avoid deadlock.
           Pack and send data to the peers which are ready, and unpack the data
           when the data are ready. Send and receive concurrently.
oshmem_v4: Use double buffers strategy to avoid checking the peer's buffer status.
           Send data to the peer's two receiving buffers alternatively in different steps.
oshmem_v5: Remove pack(). Send scattered data to the peer with many shmem_put. 
oshmem_v6: Combined v5 and v1. Remove pack() and use MPI_Barrier in remap_3d().

# Related modfied files list:
1. main.cpp
2. globals.cpp, globals.h  
3. memory.cpp, memory.h   
4.remap.cpp, remap.h   
5. remap_wrap.cpp, remap_wrap.h 
6. fft3d.cpp, fft3d.h
7. pppm.cpp, pppm.h   

## Update 7/20/2015
Paper was accepted by OpenSHMEM workshop 2015. The manuscript and figures
are stored in folder paper_shmemWorkshop2015.

## Update 10/1/2014
Remove unnecessary folders. They can be found in the
original LAMMPS package.

# Ways to compile
To compile the LAMMPS on Titan@ORNL, take the following steps:
1. Download the latest version of LAMMPS: http://lammps.sandia.gov
  * The current latest version is 28Jun2014. It is the one I worked on.
  * I used to work on an older version 1Feb2014. There might be bugs
  * in the codes about memory allocation. I can't reach more than 4k 
  * processes for either strong scaling or weak scaling.

2. Clean all the temporal files and remove all the package.
My version is to run the protein (in.rhodo) benchmark on titan. 
Proteins need packages of molecule, space, rigid on titan. 
Modify the makefie /MAKE/makefile.xt5titan. 
Change the compiler to CC. 
Command list is shown below:

make clean-all
make no-all
make package-status  //to check the package status
make yes-molecule
make yes-kspace
make yes-rigid
make xt5titan -j
(flag "-j"   is fast for parallel compiling.)

# Example script to run lammps

#!/bin/bash
export XT_SYMMETRIC_HEAP_SIZE=1500M
SCALEx=16
SCALEy=16
SCALEz=16
let PROCS=${SCALEx}*${SCALEy}*${SCALEz}
aprun -n ${PROCS} -j 1 ./lmp_xt5titan_shmem_v4 -v x ${SCALEx} -v y ${SCALEy} -v z ${SCALEz}  -in in.rhodo.scaled


