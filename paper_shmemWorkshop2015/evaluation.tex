\section{Evaluation} \label{ch:evaluation}

\subsection{Experimental Setup} 

During the evaluation, we perform both strong and weak scaling
experiments. In both cases we consider the \emph{rhodopsin protein
simulation} input problem. For the strong scaling case, the input
problem size remains constant at $8\times 8\times 8\times 32$K atoms. For
the weak scaling tests, the input problem size is set proportionally to the 
number of PEs, so that each processor handles a load of 32K atoms.

The evaluations are performed on Titan, a Cray XK7 supercomputer
located at ORNL (with Cray-MPICH 6.3.0 and Cray-shmem 6.3.0 software
stacks). On this machine, even when two different allocations request
the same number of nodes, they may be deployed on different physical
machines, connected by a different physical network topology. This
has been known to cause undesired performance variability that
prevents directly comparing the performance obtained from different
allocations. We eliminate this effect by comparing MPI versus \oshmem
on the same allocation, and by taking averages over 10 samples of
each experiment. We present the total time of the LAMMPS application
with error bars to illustrate any increase (or lack thereof) in the
performance variability, and we then present the speedup of the
considered \oshmem enhanced variants over the MPI implementation.

\begin{figure}
    \centering
    \subfloat[Strong Scaling\label{fig:lammps_v16:st}.]{\includegraphics[width=.49\linewidth]{lammps-stscale-nopack}}\hfill
    \subfloat[Weak Scaling\label{fig:lammps_v16:wk}.]{\includegraphics[width=.49\linewidth]{lammps-wkscale-nopack}}
    \caption{Total LAMMPS execution time comparison between the following versions: original MPI, \vhbarrier, 
    and \vnopack (with and w/o non-blocking \shput).\label{fig:lammps_v16}}
\end{figure}

\subsection{Comparison between the \vhbarrier and MPI versions}

Figure~\ref{fig:lammps_v16} presents the strong scaling and weak
scaling of LAMMPS for the original version where the \remap function
is purely implemented with MPI, and the two hybrid versions
(\vhbarrier and \vnopack) where the \remap function features \shput
communication and \oshmem based communication completion signaling; 
yet the protection against premature inter-iteration data overwrite
relies on an \mpibarrier synchronization. The first observation
is that the standard deviation of results is very similar between all
versions. The conversion to \oshmem has not increased the performance
variability. In terms of speedup, the \vhbarrier
version enjoys better performance than the MPI version in all cases.
The strong scaling experiments demonstrate an improvement from 2\% at
512 PEs, to over 9\% at 4096 PEs, which indicates that the \oshmem APIs
indeed accelerate the whole communication speed by amortizing the
cost of multiple communication in each synchronization. 
This is mainly due to the per-communication cost of synchronization; 
in the original loop over \mpisend, each individual communication 
synchronizes with the matching receive, in order; in contrast, and despite the coarse granularity of the 
\mpibarrier synchronization, which may impose unnecessary wait times between imperfectly 
load balanced iterations, the cost of synchronizing in bulk numerous 
\shput operations to multiple targets is amortized over more 
communications and permits a relaxed ordering between the \emph{puts}. 
Even in the weak scaling case, in which the communication speed
improvement is mechanically proportionally diminished, the \vhbarrier
version still outperforms the MPI version by 4\% at 4096 PEs, and as
the system scale grows, the performance benefit of \oshmem
increases, which indicates that the \oshmem programming approach has
the potential to exhibit better scalability. One has to remember that 
these improvements are pertaining to the modification of a single 
function in LAMMPS, which represents approximately 30\% of the 
total communication time, meanwhile the communication time is only 
a fraction of the total time. Such levels of communication performance 
improvements are indeed significant, and would be even more pronounced 
should all MPI operations, which are still accounting for the most 
communication time in the hybrid version of LAMMPS, were ported to \oshmem. 

\subsection{Consequences of Avoiding Packing}

Figure~\ref{fig:lammps_v16} also compares the \vnopack version to the
MPI version. When the default environment respects the full, blocking
specification for \shput operations (\dmappnbi{0}), the performance
of the \vnopack version is reduced compared to both the MPI and 
\vhbarrier versions, which both pack the data. With this
strict semantic, the injection rate for small messages is limited by
the wait time to guarantee that the source buffer can be reused
immediately. On the Cray system, the parameter (\dmappnbi{1}) permits
relaxing the \shput operation semantic: the \shput operations are
allowed to complete immediately, but the source buffer can be
modified only after a subsequent synchronization operation (like a
\shquiet) has been issued. With this option, neither the MPI nor the
\vnopack versions performance are modified (an expected result, that 
parameter has no effect on MPI, and the \vhbarrier version exchanges
large messages, and is therefore essentially
bandwidth limited). However, the \vnopack version's performance
improves tremendously, as the injection rate for small messages is
greatly increased. This observation provides strong evidence that
the addition of a non-blocking \shput could result in significant
benefits for some application patterns with a large number of small
size message exchanges. However, as is illustrated in the \remap 
function, the packing strategy proves to be an effective workaround: even 
with non-blocking puts, the \vnopack version closely matches
the performance of the \vhbarrier version.

\begin{figure}
    \centering
    \subfloat[Strong Scaling\label{fig:lammps_v23:st}.]{\includegraphics[width=.49\linewidth]{lammps-stscale-ck}}\hfill
    \subfloat[Weak Scaling\label{fig:lammps_v23:wk}.]{\includegraphics[width=.49\linewidth]{lammps-wkscale-ck}}
    \caption{Total LAMMPS execution time comparison between the following versions: original MPI, \vchkbuf, 
    and \vachkbuf.\label{fig:lammps_v23}}
\end{figure}

\subsection{Performance when Eliminating Group Barriers} The goal of
the \vchkbuf and \vachkbuf 
%and \vdbuf 
versions is to eliminate the group
synchronization, employed in the \vhbarrier version, that avoid the premature
overwrite of the \scratchb by a process that has advanced to the next
iteration. Their performance is compared to the MPI version in
Figure~\ref{fig:lammps_v23}. The strong scaling of these versions
(Figure~\ref{fig:lammps_v23:st}) generally perform better than the
MPI version, with a 2\% improvement from 512 PEs, and upto 5\% for
4096 PEs. In the weak scaling case, the benefit manifests only for
larger PEs counts, with a 3\% improvement.

However, when comparing to the \vhbarrier version, presented in the
previous Figure~\ref{fig:lammps_v16}, no further performance
improvement is achieved. Although the barrier has
been removed, it is likely that the introduction of synchronization
between pairs of peers still transitively synchronize these same
processes. In addition, the busy waiting loop on the peer's buffer
status scans the memory and increases the memory bus pressure. It finally
appears that, in the \oshmem model, trying to optimize the scope of
synchronizations can result in decreasing performance, by
synchronizing at too fine a granularity, which actually mimics the
traditional implicit synchronization pattern found in MPI two-sided
applications. 


\subsection{Discussion} This porting effort has led to a few 
observations on the entire process. Today, MPI is considered as the
de-facto programming paradigm for parallel applications, and PGAS
type languages are still only challengers. Shifting from one
programming model to another is not an easy task: it takes time and
effort to educate the developers about the new concepts, and then
correctly translate the application from one programming paradigm to
another.

\paragraph*{The Good:} Although MPI is a more stable, well-defined
API, PGAS based approaches have clear advantages
for some types of usage patterns. Until \oshmem reaches the same degree of flexibility and
adaptability, being able to compose the two programming paradigms in
the context of the same application is a clear necessity. Our
experience confirmed that it is indeed possible to mix the
programming models, and to upgrade an application in incremental
steps, focusing on performance critical routines independently in
order to take advantage of the strong features of each model, and
improve the performance and scalability of a highly-optimized MPI
application.

An area where the PGAS model exhibit a clear advantage over MPI is
the handling of unexpected messages. when MPI applications get
out of sync due to imbalance in the workload or system noise, most
messages become unexpected, forcing the MPI library to buffer some
data internally to delay the delivery until the posting of the
corresponding receive call. This temporary buffering has an impact on
the memory accesses as it implies additional memory copies and thus
extra overhead on the memory bus. As the memory is always exposed
in OpenSHMEM, even when the target is passive, this pathological
usage pattern cannot be triggered. In exchange, some of the implicit
synchronization semantic carried by the send-recv model is lost, and
explicit synchronization becomes necessary.

\paragraph*{The Bad:} One of the most unsettling missing features from
the OpenSHMEM standard is the capability to work with irregular
process groups. The current group collective operations are limited
to well defined process topologies, mostly multi-dimensional cubes.
As a consequence, they lack the flexibility of their MPI counterparts
which operate on communicators and can express collective behaviors
across arbitrary groups of processes. This leads to less natural
synchronization points, especially in non-symmetric cases. 

Another missing feature is a standardized support of the non-blocking
\emph{put} capabilities of the network. Forcing the \shput operation to
ignore its strict specification and relaxing the total completion of
the operation to the next synchronization does improve tremendously
the injection rate for small messages. However, today, one has no
portable way to achieve this result in the \oshmem specification, and 
relying on packing still remains the best bet to achieve the maximum
bandwidth when the input data are scattered.

\paragraph*{The Ugly:}
One of the selling points of OpenSHMEM is the exposure at the user level of
\emph{bare metal} network primitives, with a promise for a more direct access to the
hardware and higher performance compared with more abstract programming paradigms,
such as MPI. However, the restricted API of OpenSHMEM, and especially the lack
of high level primitives such as collective communication with neighborhood
communication patterns, forces the application developers to design and
implement their own synchronization operations, making the applications
sensitive to hardware and
network topological feature variations. 
%
Furthermore, the explicit management of synchronization from the user
code (instead of the implicit synchronization carried by the
two-sided operations) can result in notable performance differences,
depending on the strategy one employs to restore the required
synchronization for a correct execution. We found that some theoretically 
promising optimizations can actually yield a negative effect on overall
performance, and that the reasons for these detrimental consequences
are often subtle and hard to predict. Overall, the balance between portability and
performance is weaker than in MPI.

