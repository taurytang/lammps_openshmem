For Fig.4, Fig.5 and Fig.6.

Original Data is stored in timeMPI.txt, timeSHMEM_v*.txt.

To obtain the data for gnuplot
1. run ./grepdata to get the timing data for different processes.
2. run ./Avgtime to get the averaged time and standard deviation.
3. run ./merge to get the MPI and SHMEM time in a file for plotting.
4. run ./clean to clean all the intermediate files.
5. run ./drawLammps*.p to plot the pdf figures.

Note:
1. In Fig4_data_v1v6  here v6 corresponds to shmem_v5 in the text.

################################################################
For Fig.2
The text file 256.txt, 512.txt, 1024.txt, 2048.txt and 4096.txt
are all extracted from mpiP output file.

Apptime represents the time for application.
MPItime represents the time for all MPI functions.

run ./Avgtime to calculate the average time.
run ./drawAvg to plot the profiling figure.

################################################################
Fig.1 and Fig.3 are copied from the slides of ppt_thesis_final_chunyan.pptx
