# Plotting the data of file with points and non-continuous lines

set term postscript eps enhanced color 
set output "lammps_weakscal_v16.eps"

set size square .66

#set title "Performance of lammps on titan"

#define axis
set style line 2 lc rgb '#000000' lt 1 lw 2   # ---black
set border  back ls 2

# set color and size of points and lines, and pointslinesinterval
#set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 pi -1 ps 1 #----blue
#set style line 1 lc rgb '#8a2be2' lt 1 lw 2 pt 7 pi -1 ps 1.5 #----blueviolet

#set style line 1 lc rgb '#ff0000' lt 1 lw 1.5 pt 7 pi -1 ps 1.5 #----red
#set pointintervalbox 2

# define grid
set style line 12 lc rgb '#7f7f7f' lt 0 lw 1 # -----gray
set grid back ls 12


set style line 3 lc rgb "#000000" lt 2 lw 2
set style line 4 lc rgb "#0060ad" lt 1 lw 2 pt 8
set style line 5 lc rgb "#8a2be2" lt 1 lw 2 pt 2
set style line 6 lc rgb "#000000" lt 1 lw 2
set style line 7 lc rgb "#0000FF" lt 1 lw 2 pt 4    #---blue
set style line 8 lc rgb "#006400" lt 1 lw 2    #---green
set style line 9 lc rgb "#FF0000" lt 1 lw 2 pt 5  #---red
set style line 10 lc rgb "#6A5ACD" lt 1 lw 2    #---slateblue
set style line 11 lc rgb "#EE82EE" lt 1 lw 2    #---violet
set style line 12 lc rgb "#FF8C00" lt 1 lw 2   #---darkorange

# set position of title in the plotted image
set key bottom left  

set xlabel "Number of Processes"
set ylabel "Time Elapsed (sec)"
set y2label "Time Ratio"

set xtics ( "8" 1, "64" 2, "512" 3, "4096" 4)
set xrange [0.5:4.5]
set yr [0:200]
set ytics 20 
set y2r[0.8:1.2]
set y2tics 0.04

overhead(x, y)=x/y
error(x, dx, y, dy)=(x/y) * sqrt( (dx/x)**2.0 + (dy/y)**2.0 )

   plot "time_std_v1.txt"  using 1:3:($4) title "MPI" with yerrorbars ls 5 axes x1y1,\
        "" using 1:3 title "" with lines ls 5 axes x1y1, \
        "" using 1:5:($6) title 'Hybrid\_Barrier' with yerrorbars ls 4 axes x1y1,\
        "" using 1:5 title "" with lines ls 4 axes x1y1, \
        "" using 1:( overhead($5, $3) ):(error($5, $6, $3, $4)) title 'Hybrid\_Barrier/MPI' with yerrorbars ls 6 axes x1y2, \
        "" using 1:( overhead($5, $3) ) title "" with lines ls 6 axes x1y2,\
        1 w lines ls 3 axes x1y2 t "",\
        "time_std_v6.txt" using 1:5:($6) title 'Hybrid\_noPack' with yerrorbars ls 9 axes x1y1,\
        "" using 1:5 title "" with lines ls 9 axes x1y1, \
        "" using 1:( overhead($5, $3) ):(error($5, $6, $3, $4)) title 'Hybrid\_noPack/MPI' with yerrorbars ls 12 axes x1y2, \
        "" using 1:( overhead($5, $3) ) title "" with lines ls 12 axes x1y2,\
        1 w lines ls 12 axes x1y2 t ""



!epstopdf lammps_weakscal_v16.eps && rm lammps_weakscal_v16.eps
