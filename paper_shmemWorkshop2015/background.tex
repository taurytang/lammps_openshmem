\section{Background} \label{ch:background}
This section presents the target application, LAMMPS. It also
presents a summary explaining the major differences between \oshmem
and MPI.

\subsection{LAMMPS}
Large-scale Atomic/Molecular Massively Parallel Simulator (LAMMPS) is
a classical molecular dynamics (MD) code developed by Sandia National
Laboratories\footnote{\url{http://lammps.sandia.gov/}}. In essence, LAMMPS integrates
Newton's equations of motion with a variety of initial and/or
boundary conditions. The Newton's equations calculate the motion
of collections of atoms, molecules, or macroscopic particles that
interact via short- or long-range forces~\cite{plimpton95}.

\begin{figure}[htp]
 \center
 \includegraphics[width=.75\linewidth]{3dfft_decom.pdf}
 \caption{Parallel 3D-FFT in 2D decomposition.}
 \label{fig:3dfft_decom}
\end{figure} 

To compute the long-range Coulomb interactions, LAMMPS employs 
the three dimensional fast Fourier transform (3D-FFT) package, 
a transpose based parallel FFT  
developed by Steve Plimpton~\cite{3dfft}.
Transpose-based FFT performs one dimension of 1D-FFT at a time, and transposes
the data grid when needed.
The 2D decomposition of parallel 3D-FFT is shown in Figure~\ref{fig:3dfft_decom}. 
The three dimensions of the real space 
are labeled as Slow, Mid, and Fast axes, respectively.
 A data grid of size $l\times m\times n$  
is divided between $1\times p\times q$ processes. Hence the split size 
of the sub-domain is \textit{nfast} = \textit{l}, \textit{nmid} = \textit{m/p}, 
and \textit{nslow} = \textit{n/q}.
Each process contains a sub-domain (pencil) of the data. 
First, each process performs the 1D-FFT along the Fast axis, where the data
 on this direction are local. The processes then transpose the Mid and Fast axes 
 in order to perform the local 1D-FFT along the Mid axis.
 Finally, processes transpose the Mid and Slow
 axes and then perform a 1D-FFT along the Slow axis.

As an initial step to the analysis, we collected statistical
information about MPI usage in LAMMPS with the MPI profiling tool (mpiP~\cite{Vetter:2001:SSA:568014.379590}).
We used the \emph{rhodopsin} test case (as provided by the LAMMPS test suite). 
The input file for this benchmark provides a basic problem size of 32K atoms. 
The problem size for profiling is scaled to $8\times 8\times 8\times 32$K atoms.
%
%The profiled result is shown in Figure~\ref{fig:mpiP_profiling}, where MPITime is the time spent in MPI functions, and computation is everything else.
In this strong scaling benchmark, the portion of time spent in MPI 
function over the total application time
is 5.5\% for 256 processes, 12.6\% for 512 processes, 
18.1\% for 1024 processes, 25.4\% for 2048 processes and
  38.5\% for 4096 processes, respectively.
Clearly, communication is a dominating factor in the overall performance,
therefore reducing the communication time has the potential to improve 
LAMMPS performance significantly at scale. 

%\begin{figure}[htp]
%\center
%includegraphics[scale=0.68]{avgLammps.pdf}
%\caption{LAMMPS profiling by mpiP.}
%\label{fig:mpiP_profiling}
%\end{figure} 

In this test case, LAMMPS has 167 sites with calls to MPI functions.
Doing a complete port of MPI-based LAMMPS to \oshmem is a large undertaking
which is unlikely to be completed in short order. Instead we chose 
to focus our efforts on the most data exchange intensive site, 
the \remap function, which is responsible for the transpose operations 
in the 3D-FFT. The \remap function employs a set of MPI
point-to-point operations (\mpisend, \mpiirecv, \mpiwaitany) which concentrate
the largest share of the overall transmitted MPI byte volume
(32.45\% for 4096 processes).
Consequently, any communication
performance improvement in this function is expected to yield a sizable overall
acceleration.

The resulting application is a hybrid MPI-\oshmem application, where the legacy MPI application is
accelerated with \oshmem communication routines in performance critical sections.

\subsection{OpenSHMEM vs. MPI}
To transform the above MPI-based implementation into one based on
\oshmem, we must account for four salient differences between the
two programing models.

\paragraph*{Address space differences:} MPI considers a distributed
address space, where each process has a private memory space, in
which addresses are purely local. In contrast, \oshmem features a
partitioned global address space, in which each processing element (PE) has a private
memory space, but that memory space is mapped to symmetric addresses,
that is, if a program declares a global array \texttt{A}, the 
local address of the element \texttt{A[i]} is the relevant parameter 
in a communication call to target \texttt{A[i]} at every PE. The main advantage of the latter
model is that it enables the origin process in the operation to 
compute all the remote addresses for the operation, which it can 
then issue directly without first converting the addresses to the 
target's address space. This is a major feature that eases the 
development of applications using the one-sided communication paradigm. 

\paragraph*{Communication semantics differences:} \oshmem is based on
one-sided communication. This is in contrast to the two-sided
communication employed in MPI LAMMPS. Although MPI has provided one-sided
communication since MPI-2 (i.e., \mpiget and \mpiput), two-sided
communication programs dominate the common practice of using MPI. In
two-sided operations, every Send operation matches a Receive operation.
Each process provides a buffer, in the local address space, in which
the input or output are to be accessed. In contrast, in the one-sided
model, communications are issued at the origin, without any matching
call at the target. Therefore, the operation progresses without an
explicit action at the target, which is then considered to be
\emph{passive}, and, the origin PE must be able to perform all the
necessary steps to issue and complete the communication without
explicit actions from the target. In particular, the target doesn't 
call an operation to provide the address of the target buffer, so 
the origin must be able to locate this memory location (which is 
usually simple, thanks to the symmetric address space).

\paragraph*{Synchronization differences:} Although MPI provides an
explicit synchronization operation (\emph{i.e.}, \mpibarrier),
most MPI two-sided communication operations carry an implicit
synchronization semantic. As long as a process does not pass a
particular memory location as a buffer in one of the MPI
communication functions, the process knows it enjoys an exclusive use
of that memory location, and it cannot be read from, nor written to, by
any other process, until it is explicitly exposed by an MPI operation.
Similarly, when the Send or Recv operations complete, the process
knows that any exposure of that memory has ceased, and once again
that memory can be considered exclusive. However, this is not true in the
one-sided communication model, where a process can be the target
of an operation at any time and the symmetric memory is exposed by default, at
all times. As a consequence, when a process performs a 
\shput\footnote{For brevity, in this paper we use the 
simplified nomenclature \shput, \shget, which are not actual \oshmem functions, 
but refer to the actual typed operations (like \texttt{shmem\_double\_put}, \texttt{shmem\_long\_get}, etc.)}, it 
cannot rely on a corresponding \mpiirecv to implicitly synchronize
on the availability of the target buffer, and that synchronization
must now be explicit. \oshmem indeed provides a more flexible set of
explicit synchronization operations, like \shfence, which
guarantees ordering between two operations issued at that origin;
\shquiet, which guarantees completion of all operations
issued at that origin (including remote completion); and
\verb+shmem_barrier_*+, a set of group synchronizations that also
complete all operations that have been posted prior to the barrier.
Although the explicit management of synchronization can be complex,
compared to the implicit synchronizations provided by the two-sided
model, the cost of synchronizing can be amortized over a large number
of operations, and thereby has the potential to improve the
performance of applications that exchange a large number of small
messages.

\paragraph*{Collective operations differences:} MPI proposes a fully
featured interface to manage fine grain grouping of processes that
participate in communication operations. The communicator concept
permits establishing arbitrary groups of processes which can then
issue communication operations (collective or otherwise) that span
that subgroup only. In contrast, \oshmem attempts to avoid having to
perform message matching to reduce the latency, and consequently does
not have such fine-grained control over the group of processes
participating in a collective operation. \oshmem features two kinds
of collective operations, those that involve all PEs (like
\texttt{shmem\_barrier\_all}), and those that involve a structured
group of PEs, in which processes involved in the communication are
those whose identifier is separated by a constant stride, which must
be a power of 2. This lower flexibility in establishing groups of
processes on which collective operations can operate can cause
additional complications. Of particular relevance is
the case where a collective synchronization is needed. Because the
\oshmem group synchronization operations are operating on groups that
do not necessarily match the original grouping from the MPI
application (like the \emph{plans} in the \remap function), one is forced to
either 1) use point-to-point synchronizations to construct one's own
barrier that maps the PE group at hand, which could prove more
expensive than an optimized barrier operation; or 2) use a global
synchronization operation that spans all PEs, thereby synchronizing
more processes than are necessary, with the potential outcome of
harming performance with unnecessary wait time at theses processes.
