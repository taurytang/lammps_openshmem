\section{Methodology}\label{ch:Method}

In this section we expose the challenges we have faced when porting the 
\remap LAMMPS function from MPI to \oshmem. We then discuss a number 
of optimization strategies for the explicit management of process 
synchronization. 

\subsection{MPI features in the \remap function}

The \remap function we focus on in this work employs non-blocking 
communication primitives to exchange
data between MPI processes of a particular subgroup.
Peers and message sizes are calculated and obtained from the
remap \emph{plan} data structure.
Each \emph{plan} corresponds to a subset of processes (in each direction) 
and is mapped to an MPI communicator. 
Three nested \texttt{for} loops enclose the \mpiirecv, \mpisend, and
\mpiwaitany calls and form a many-to-many, irregular communication pattern. 
Of particular interest is the usage of the \mpisend function in the original 
code, which sequentializes the 
multiple \emph{sends} from a process, thereby creating a logical ordering
 and implicit synchronizations between groups
of processes. 

During the 3D-FFT transpose, the data to be transferred for each PE comes 
from the \emph{in} buffer, however from non-contiguous memory locations.
The MPI implementation packs the scattered data to a contiguous buffer 
before performing the send operation.
Similarly, the receive process stores the incoming data in a 
temporary contiguous \scratchb as well. Only after the 
\mpiirecv is issued can the 
data reach the target \scratchb. The received data is
then unpacked to the corresponding locations in the \emph{out} buffer.

\begin{figure}[htp]
 \centering
\parbox{.48\linewidth}{
 \centering
 \includegraphics[width=\linewidth]{mpi_pp}
}\quad\parbox{.48\linewidth}{
 \centering
 \includegraphics[width=\linewidth]{shmem_pp}
}
 \caption{Communication pattern between two processes, with MPI (left),
  and \oshmem (right). For simplification, in the figure, a process is shown as 
 having an exclusive role; in reality every process is both
 a sender and a receiver with multiple peers during each FFT iteration.}
 \label{fig:mpishmem_pp}
\end{figure} 

\subsection{Initial Porting Effort} In this section we discuss the
 porting effort to create a basic \oshmem code which is functionally
equivalent to the original MPI \remap function. Different 
performance optimizations will be presented in later sections.

\paragraph*{From two-sided to one-sided:} The two-sided \mpisend and
\mpiirecv pair can be transformed into a one-sided operation by either issuing
a \shput at the sender, or a \shget at the receiver. In
this test case, \shput is selected because it improves the
opportunity for communication and computation overlap. With the \shget
semantic, the operation completes when the output buffer has
been updated (that is, the communication has completed). Essentially,
\shget is a blocking operation, and there are no non-blocking
variants in the current \oshmem specification. Therefore, in a single
threaded program, \shget leaves little opportunity for initiating
concurrent computations. The \shput semantic is more relaxed: the
\shput operation blocks until the input buffer can be reused at the
origin. It does not block until the remote completion is achieved at
the target, which means that the operation may complete while
communication is still ongoing (especially on short messages).
More importantly, the approach closely matches the original MPI
code logic, in which computation is executed at the receiver after
posting non-blocking \mpiirecv operations so as to overlap the
communication progress, meanwhile blocking sends are used, which have
the same local completion semantic as \shput operations.

\lstset{tabsize=2, numbers=left, language=C, basicstyle=\scriptsize}

\begin{lstlisting}[frame=tb,float,
caption={Allocating the \scratchb{}s in the Partitioned 
Global memory space, as needed for making them targets in \shput.},captionpos=b,label=listing4] 
...
shmem_int_max_to_all(&scratch_size, &scratch_size, 1, 0, 0, nprocs,
                     pWrk1, pSync1);
plan->scratch = (FFT_DATA *) shmem_malloc(scratch_size*sizeof(FFT_DATA));
...
\end{lstlisting}

\paragraph*{Allocating the symmetric scratch buffers:} In \shput, the
target data buffer (the \scratchb in our case) must be globally
addressable. Hence, the memory space referenced by
\verb+plan->scratch+ is allocated in the partitioned global address
space of \oshmem (line 4 in Listing~\ref{listing4}). The user is
responsible for ensuring that the \verb+shmem_malloc+ function is called
with identical parameters at all PEs (a requirement to ensure that
the allocated buffer is indeed symmetrical). As a consequence,
although processes may receive a different amount of data during
the irregular communication pattern, the \scratchb must
still be of an identical size at all PEs, which must be sufficient 
to accommodate the maximum size across all PEs. This concept simplifies 
the management of the global memory space at the expense of potentially increasing the
memory consumption at some PEs. Another benign consequence is the need
to determine, upon the creation of the \emph{plan} structure, the largest
memory space among all PEs (line 2 in Listing~\ref{listing4}).


\begin{lstlisting}[frame=tb,float,
caption={Exchanging the offsets in the target scratch buffers; 
a parameter to \shput that was not required with \mpisend.},captionpos=b,label=listing3] 
...
plan->remote_offset = (int *) shmem_malloc(nprocs*sizeof(int));
for( i = 0; i < plan->nrecv; i++)
  shmem_int_p(&plan->remote_offset[me], plan->recv_bufloc[i], 
              plan->recv_proc[i]);
shmem_fence();
...
\end{lstlisting}

\paragraph{Irregular communication patterns and target offsets:}
Except for the above communication semantics difference, most of the
communication parameters at the sender remain the same (\emph{e.g.},
send buffer address and size, target peer processes). A notable
exception is that, unlike in the two-sided model in which the
receiver is in charge of providing the target buffer address during
the \mpiirecv, in the \oshmem version it must be known at the origin
before issuing the \shput. Although the \scratchb{}s are
in the symmetric address space, and it is therefore simple to compute
the start address of this buffer, the particular offset at which a
process writes into the \scratchb is dependent upon the
cumulative size of messages sent by processes whose PE identifier is
lower. As the communication pattern is irregular, that offset is not
symmetric between the different PEs and cannot be inferred at the 
origin independently. Ultimately, this complication stems from the different
synchronization models between one-sided and two-sided operations: as
the sender doesn't synchronize to establish a rendezvous with the 
receiver before performing the remote update, in the one-sided model, the
target address must be pre-exchanged explicitly.  Fortunately,
we noted that, in the \remap function, the offset in the target 
buffer is invariant for a particular \emph{plan}, hence we only need to
transmit the offset in the target buffer to the sender once, when the
\remap \emph{plan} is initially created. An extra buffer named
\verb+plan->remote_offset+ is allocated in the \emph{plan}, and used to 
persistently store the offsets in the peers target buffer locations, 
as shown in Listing~\ref{listing3}. When the same \emph{plan} is executed 
multiple times, \shput operations can thereby be issued directly without 
further exchanges to obtain the target offset. 

\paragraph*{Signaling \shput operations completion:} The
original MPI program can rely on the implicit synchronization carried
by the explicit exposure of the memory buffers between the \mpiirecv,
\mpisend, and \mpiwaitany operations in order to track completion of
communication operations. In the \oshmem-based implementation, the
target of a \shput is passive, and cannot determine if all the data 
has been delivered from the operation semantic only. To track the
completion of the \shput operations, we add an extra array
\verb+plan->remote_status+ which contains the per origin readiness of
the target buffer. The statuses are initialized as \texttt{WAIT} before any
communication happens. After the \shput, the sender uses a \shintp to
update the status at the target to \texttt{READY}. A \shfence is used between
\shput and \shintp to ensure the order of remote operations. The
target PEs snoop the status memory locations to determine the
completion of the \shput operations from peers.

\paragraph*{Signaling target buffer availability:} It is worth noting
that the same remap \emph{plan} is used multiple time during the
3d-FFT process. Hence the scratch buffer could be overwritten by the
next \shput, if the sender computes faster and enters the next
iteration before the data is unpacked at the receiver. It is
henceforth necessary to protect the scratch buffer from early
overwrite. In the initial version, we implemented this receive buffer
readiness synchronization in \oshmem with an \mpibarrier (this
version of the code thereafter referred to as
\vhbarrier). We use an \mpibarrier rather than any of the
\verb+shmem_barrier_*+ functions because we need to perform a synchronization
in a subgroup whose shape is not amenable to \oshmem group
operations, while \mpibarrier is able to force a synchronization on an
arbitrary group. Figure~\ref{fig:mpishmem_pp}
reflects the schematic structure of applying \shput for
point-to-point communication in \remap.

An arguably more natural way of preventing that overwrite would be to
synchronize with \shintp when the receiver scratch buffer can be
reused, and have the sender issue a \shwait to wait
until that target buffer becomes available. However, each process
updates the scratch buffers at multiple targets, and this strategy
would result in ordering these updates according to the code flow
order, rather than performing the \shput operations opportunistically
on the first available target. Hence, it would negate one of the
advantages over the traditional \mpisend based code, without making
the overlap of computation and communication more likely when uneven
load balance can make some targets significantly slower than others.
We will discuss alternative approaches that avoid this caveat in the 
optimization section,~\ref{sec.advchkbuff}.

\subsection{Optimizations}

\paragraph*{Non-blocking \shput to avoid packing data:} As
mentioned above, the MPI implementation of LAMMPS packs the
non-contiguous data into a contiguous buffer before sending. A root
reason comes from the higher cost of sending multiple short MPI
messages compared to a single long one. The one-sided model, and the
decoupling of the transfer and synchronization offer an opportunity
for reaching better performance with \oshmem when multiple small
messages have to be sent, henceforth opening the possibility to
directly transfer small, scattered data chunks without first copying
into an intermediate pack buffer. As an example, \shput demonstrates
a great performance advantage for sending multiple small messages
when it is implemented over the \texttt{dmapp\_put\_nbi} function, a
non-blocking implicit \emph{put} in the Cray DMAPP
API~\cite{dmapp2015}.

Still based on the \vhbarrier, a new version (called
\vnopack) removes the data packing. Each PE transfers the
scattered data of size \textit{nfast} to its peers directly, without
resorting to an intermediate pack buffer. To transfer a full
sub-domain of size $\textit{nslow}\times \textit{nmid}\times
\textit{nfast}$, a number of $\textit{nslow}\times \textit{nmid}$
\shput operations are required.

\paragraph*{Eliminating synchronization
barriers:}\label{sec.advchkbuff} In the \vhbarrier version, MPI
point-to-point communications are replaced with \oshmem one-sided
communication semantics. To enforce synchronization between a sender
and a receiver, a polling mechanism is employed. However, even with
such a mechanism, an additional synchronization, in the initial \oshmem
version implemented with an \mpibarrier, is still needed to prevent
the next iteration from issuing \shput and modifying the dataset on
which the current iteration is computing. This barrier could cause some
performance loss, especially at large scale or when the load is not
perfectly balanced.

In the \vchkbuf version, the barrier is replaced by a
fine grain management of the availability of the \scratchb. A pair 
of communication statuses, \texttt{WAIT} and \texttt{READY}, are introduced
to mark the status of the receiver's \scratchb. A new
array \verb+plan->local_remote_status+ stores, at the sender, the
statuses of target receive buffers at all remote PEs. Before issuing a
remote write on a particular PE's \scratchb, the
origin checks the availability status of the buffer in its local
array. If the status is still set to \texttt{WAIT}, the \shput is delayed to a
later date when, hopefully, the target PE will have unpacked the
\scratchb, thereby making it safe for reuse. When the
target finishes unpacking the \scratchb, it remotely
toggles the status at the sender to \texttt{READY} (with a \shintp operation),
to inform the sender that it may start issuing \shput operations
safely.

These synchronizations mimic the establishment of rendezvous between
two-sided matching send-recv pairs. However, the MPI two-sided
rendezvous can start at the earliest when both the sender and the
receiver have posted the operations. In contrast, in the one-sided
version, the receiver can toggle the availability status much
earlier at the sender, as soon as the \emph{unpack} operation is completed, which
makes for a very high probability that the status is already set to
\texttt{READY} when the sender checks for the availability of the target buffer.
Unfortunately, we found that on many \oshmem implementations, the
delivery of \shintp may be lazy, and delayed, negating the
aforementioned advantage. The simple addition of a \shquiet call
after all \shintp have been issued is sufficient to force the
immediate delivery of these state changes, and henceforth improves
the probability that the state is already set to \texttt{READY} when a sender
checks the status.

\paragraph*{Opportunistic Unpacking:} In the \vchkbuf
implementation, a PE unpacks its incoming data only after it
completed the puts for all the outgoing data. The slightly modified
\vachkbuf version embraces a more dynamic, opportunistic 
ordering of the \emph{unpack} operations. After issuing the 
\shput targeting the ready peers, the sender skips the peers which 
are still marked as in state \texttt{WAIT}, and instead switches to 
the task of unpacking the available incoming data. Before the next 
iteration, the sender then checks if it still has un-issued \shput operations 
from the current \emph{plan} and satisfies 
them at this point. A major advantage of this method 
is that it overlaps the rendezvous synchronization wait time at the 
sender with data unpacking. 

A design feature in the LAMMPS communication pattern, however, adds
some complexity to the \vachkbuf strategy. In the same
\remap, the \emph{in} and \emph{out} buffers could point to the
same memory blocks, in certain cases. This is not an issue when a PE
sends out (or packs) all the outgoing data from the \emph{in} buffer
before it starts unpacking the received data from the \scratchb.
However, in the \vachkbuf strategy, the unpack
operation can happen before \shput. A memory block from the
\emph{in} buffer, where the data is waiting to be transferred to a
slow PE, could thereby be mistakenly overwritten by the unpacking of
the \scratchb, touching that same block through the aliased pointer 
in the \emph{out} buffer. This unexpected memory sharing between the two
data buffers obviously threatens program correctness. To resolve this
cumbersome sharing, in the conditional case where that behavior
manifests, a temporary duplicate of the \emph{out} buffer is
allocated, and the unpack happens in that copy instead. The next
iteration of the 3D-FFT will then consider that buffer as the input
memory space, and the original buffer is discarded when the iteration
completes (hence the \shput operations have all completed as well).

\begin{comment}
\paragraph*{Double buffering:} In LAMMPS, the 3D-FFT process is
iterative, and multiple repetitions of the loop are repeated until
the result reaches some accuracy or timing threshold (under the user
control). This is the basic reason justifying the use of synchronization
barrier to prevent fast processes from issuing one-sided updates 
pertaining to the next iteration while a slower process is still 
reading the target buffers in the previous iteration. It is notable 
that a particular PE can be at most one step in advance or backward 
compared to its neighboring peers, because the bidirectional data 
exchange impose this loose synchrony. 

In order to completely eliminate the extra communication time imposed
by the MPI barrier or the peer status checking, deployed respectively
in the \vhbarrier and \vchkbuf versions,
we designed a double buffer strategy (\vdbuf).
Each PE allocates two \scratchb{}s of equal size. 
At each iteration, the sender alternates the target \emph{scratch} 
buffer, so that it cannot overwrite the odd iteration buffer, possibly 
still in use, while it is issuing communication for an even iteration. 
This method simplifies the data structure and eliminate completely 
the extra synchronizations to insure the availability of the target 
receive buffer, at the expense of doubling the \emph{scratch} 
buffers memory requirements. This strategy is also exclusive with the 
\vnopack strategy. 
\end{comment}
