%\documentclass[runningheads,a4paper]{llncs}
\documentclass[a4paper]{llncs}

\usepackage{makeidx}
\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}
\usepackage{url}
\usepackage{color}
\usepackage{ifthen}
\newboolean{showremark}
\setboolean{showremark}{true}
%\setboolean{showremark}{false}
\usepackage{array}
\usepackage{caption} 
\captionsetup[table]{skip=10pt}
\usepackage{subfigure}% for multiple figures in one big figure env
\usepackage{xspace}
\usepackage{todonotes}

\ifthenelse{\boolean{showremark}}
{
\newcommand{\rem}[1]{  
                      \textcolor{magenta}{
                      {\small \#\#
                              {\sf Remark: #1} 
                      }}} 
}
{
\newcommand{\rem}[1]{}
}

\newcolumntype{\$}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
  #1\ignorespaces
}

\newcommand{\mpiirecv}[0]{\texttt{MPI\_Irecv}\xspace}
\newcommand{\mpisend}[0]{\texttt{MPI\_Send}\xspace}
\newcommand{\mpiisend}[0]{\texttt{MPI\_Isend}\xspace}
\newcommand{\mpiwaitany}[0]{\texttt{MPI\_Waitany}\xspace}
\newcommand{\mpiget}[0]{\texttt{MPI\_Get}\xspace}
\newcommand{\mpiput}[0]{\texttt{MPI\_Put}\xspace}
\newcommand{\remap}[0]{\texttt{remap\_3d()}\xspace}
\newcommand{\shput}[0]{\texttt{shmem\_put}\xspace}
\newcommand{\shget}[0]{\texttt{shmem\_get}\xspace}
\newcommand{\oshmem}[0]{OpenSHMEM\xspace}

%\urldef{\mailsa}\path|{alfred.hofmann, ursula.barth, ingrid.haas, frank.holzwarth,|
%\urldef{\mailsb}\path|anna.kramer, leonie.kunz, christine.reiss, nicole.sator,|
%\urldef{\mailsc}\path|erika.siebert-cole, peter.strasser, lncs}@springer.com|
%\newcommand{\keywords}[1]{\par\addvspace\baselineskip
%\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\title{From MPI to OpenSHMEM: Porting LAMMPS}
%Porting the LAMMPS Highly-scalable Application to the OpenSHMEM Programming Model}

%The order of the authors is to be determined ...
\author{Chunyan Tang$^{\dag}$ \and Aurelien Bouteiller$^{\dag}$  \and Thomas Herault$^{\dag}$ \and Manjunath Gorentla Venkata$^{\star}$ \and George Bosilca{\dag}}

\institute{$^{\dag}$Innovative Computing Laboratory, University of Tennessee, Knoxville \\ $^{\star}$Oak Ridge National Laboratory \\
  \{ctang7, bosilca, bouteill, herault\}@icl.utk.edu,  \{manjugv\}@ornl.gov
  }

\maketitle


\begin{abstract}
  This paper details the opportunities and challenges of porting a
  petascale-capable, MPI-based application to \oshmem.  We investigate the
  major programming challenges stemming from the differences in communication
  semantics, address space organization, and synchronization operations between
  the two programming models.  We also reveal how to solve those challenges for
  representative communication patterns in the LAMMPS application.  We then
  evaluate the performance of LAMMPS on the Titan leadership HPC system at ORNL.
  We compare MPI and \oshmem implementations in terms of both strong and
  weak scaling.  Our experience outlines that \oshmem provides a rich semantic
  to implement scalable scientific applications.  In addition, the experiments
  demonstrate that \oshmem performs and scales similarly to the optimized MPI
  implementation, and in certain configurations even outperforms the MPI
  implementation.
%\keywords{performance evaluation, OpenSHMEM, PGAS}
\end{abstract}

\input intro

\input background

\input methodology

\input evaluation

\input related_work

\input conclusions

\begin{thebibliography}{}

\bibitem{lammps:jcp1995}
 Plimpton S.: Fast Parallel Algorithms for Short-Range Molecular Dynamics. In Journal of Computational Physics, v117, p1-19, 1995. 

\bibitem{lammps:website}
LAMMPS website in Sandia National Laboratories.  
http://lammps.sandia.gov/

\bibitem{lammps:ppopp2001}
Vetter J.S. and McCracken M.O.: Statistical Scalability Analysis of Communication Operations in Distributed Applications. In Proceedings of ACM SIGPLAN Symposium on Principles and Practice of Parallel Programming (PPOPP), 2001.

%\bibitem{intel:process_variation}
%Kelin Kuhn, Chris Kenyon, Avner Kornfeld, Mark Liu, Atul Maheshwari, Wei-kai Shih, Sam Sivakumar, Greg Taylor, Peter VanDerVoorn, and Keith Zawadzki : Managing Process Variation in Intel's 45nm CMOS Technology. In Intel Technology Journal, Volume 12, Issue 2 June 17, 2008

%\bibitem{micro06:processs_variation}
%Xiaoyao Liang and David Brooks : Mitigating the Impact of Process Variations on Procesor Register Files and Execution Units.
%In IEEE/ACM International Symposium on Microarchitecture, 2006

\bibitem{pgas10:OpenSHMEM}
B. Chapman, T. Curtis, S. Pophale, S. Poole, J. Kuehn, C. Koelbel, and L. Smith : "Introducing OpenSHMEM: Shmem for the PGAS Community". In
Proceedings of the Fourth Conference on Partitioned Global Address Space Programming Model, 2010.

\bibitem{sgi:OpenSHMEM}
"SHMEM API Man Pages". [Online]. Available: http://docs.sgi.com

\bibitem{ics11:OpenSHMEM}
S.S. Pophale: SRC: OpenSHMEM Library Development. In Proceedings of International Conference on Supercomputing (ICS), 2011.

\bibitem{pgas12:npb}
Swaroop Pophale, Ramachandra Nanjegowda, Tony Curtis, Barbara Chapman, Haoqiang Jin, Stephen Poole, and Jeffrey Kuehn: OpenSHMEM Performance and Potential: An NPB Experimental Study. In Proceedings of the Sixth Conference on Partitioned Global Address Space Programming Model, 2012 

\bibitem{oug14:miniMD}
Mingzhe Li, Jian Lin, Xiaoyi Lu, Khaled Hamidouche, Karen Tomko, and Dhabaleswar K. Panda : Scalable MiniMD Design with Hybrid MPI and OpenSHMEM.
In OpenSHMEM User Group, 2014.

\end{thebibliography}



\end{document}
