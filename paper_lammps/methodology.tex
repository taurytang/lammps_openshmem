\section{Porting Methodology}
\label{sec:method}

The \remap function employs asynchronous communication primitives to exchange
data between MPI processes. Listing~\ref{listing1} illustrates the MPI usage
pattern in this function, and features a rather typical point-to-point
communication pattern commonly encountered in many HPC applications. The three
single-level of \texttt{for} loops enclosing the \mpiirecv, \mpisend, and
\mpiwaitany calls form a many-to-many irregular communication pattern. Peers and
message sizes are obtained, for a particular loop iteration, from the
\textit{plan} data structure.
% This data structure contains the number of neighbors to whom data is send to
% (\texttt{nsend}) and received from (\texttt{nrecv}), then sender/receiver rank
% (\texttt{send\_proc} and \texttt{recv\_proc}), the message size
% (\texttt{send/recv\_size}), and the memory buffer addresses (\texttt{sendbuf}
% and \texttt{recv\_bufloc[irecv]} for the sender and receiver process
% respectively).
Each plan correspond to a subset of processes (in each direction) and is defined
by an MPI communicator and the corresponding group of processes. Of particular
interest is the usage of the \mpisend function, which sequentialize the sends
from a process, and create a logical ordering or synchronization between a group
of processes.
% Additionally, the world is divided into 8 communication subdomains (selected
% with the \texttt{comm} field of the plan).

%Figure~\ref{fig:mpi_pattern} displays
%how MPI messages are exchanged between 8 processes. 
%Based on the figure, we find that it is difficult to formalize the
%communication pattern between processes.
%For example, p3 receives 4 messages and sends 4 messages,
%while p2 receives 3 messages and sends 3 messages.
%We cannot use a single statement to uniformly represent  
%communication pattern between p3 and p2.
%(\textbf{will explain more})

%The involved MPI libraries in function \remap are MPI\_Irecv, MPI\_Send and MPI\_Waitany. With a for loop for MPI\_Irecv and MPI\_Send separately, each process interacts with several other processes. The MPI interaction here is a many to many communication. The MPI code is illustrated in Fig.~\ref{fig:mpi_remap_3d}. One of the communication patterns of MPI\_Send is shown in Fig.~\ref{fig:mpi_pattern} with 8 cores.
   
% \begin{comment} 
%  \begin{figure}[htp]
%  \center
%  \includegraphics[scale=0.35]{figures/mpi_remap_3d.png}
%  \caption{MPI code for \remap function}
%  \label{fig:mpi_remap_3d}
%  \end{figure} 

%  \begin{figure}[htp]
%  \center
%  \includegraphics[scale=0.3]{figures/mpi_pattern.png}
%  \caption{MPI communication pattern}
%  \label{fig:mpi_pattern}
%  \end{figure} 
% \end{comment}

\lstset{tabsize=2, numbers=left, language=C, basicstyle=\scriptsize}
\begin{lstlisting}[frame=tb,float,
caption={MPI Communication code excerpt from \remap in LAMMPS},captionpos=b,label=listing1] 
...
for (irecv = 0; irecv < plan->nrecv; irecv++){
    MPI_Irecv(&scratch[plan->recv_bufloc[irecv]],plan->recv_size[irecv],
            MPI_FFT_SCALAR,plan->recv_proc[irecv],0,
            plan->comm,&plan->request[irecv]);
}
for (isend = 0; isend < plan->nsend; isend++){
  plan->pack(&in[plan->send_offset[isend]],
             plan->sendbuf,&plan->packplan[isend]);
  MPI_Send(plan->sendbuf,plan->send_size[isend],MPI_FFT_SCALAR,
           plan->send_proc[isend],0,plan->comm);
}
for (i = 0; i < plan->nrecv; i++){
  MPI_Waitany(plan->nrecv,plan->request,&irecv,&status);
  plan->unpack(&scratch[plan->recv_bufloc[irecv]],
             &out[plan->recv_offset[irecv]],&plan->unpackplan[irecv]);
}
  ...
\end{lstlisting}

% \begin{comment} 
% Since \oshmem is a one side communication, a single shmem\_put or
% shmem\_get can be used to replace the \mpiirecv and \mpisend pair.
% In our research we use shmem\_double\_put to take the place of
% \mpisend. When \oshmem sends data it needs to know the target,
% send size and the target buffer address. We keep the communication of
% MPI version, i.e., the target, send size and the send buffer keep the
% same. The receive buffer for shmem\_double\_put must be allocated on
% the symmetric heap. The memory size of receiver buffer must be the
% same for all processes. We used a shmem\_int\_max\_to\_all to find
% the largest size for all processes. The requirement of finding the
% largest size of all processes introduces some extra synchronization
% as indicated by the code shown in Fig.~\ref{fig:lammps_remap3d_sync}.
% The shmem\_barrier\_all is called twice in this part of code.

% \begin{figure}[htp]
%  \center
%  \includegraphics[scale=0.35]{figures/lammps_remap3d_sync.png}
%  \caption{Synchronization for shmalloc}
%  \label{fig:lammps_remap3d_sync}
%  \end{figure}
% \end{comment}

To transform the above MPI-based implementation into one based on
\oshmem, we must account for three salient differences between the
two programing models.

\textbf{Communication semantics difference.}
\oshmem is based on one-sided communication. This is in contrast
with the two-sided communication employed in MPI LAMMPS. Although MPI
provides one-sided communication (i.e., \mpiget and \mpiput)
since MPI2, two-sided communication programs dominate the common 
practice of using MPI. 

To transform the \mpisend and \mpiirecv pairs into one-sided
communications, we can replace them with either \shput at the sender,
or with \shget at the receiver. We choose to use \shput (line 5 in
Listing~\ref{listing3}), because this ensures that the communication
can start immediately, as soon as the data is available at the
sender. The data transfer will start to update the target memory at
the receiver, wether the receiver is busy computing in another code
region or not. If we use \shget, the communication is driven by the
receiver, and thereby starts only when the receiver is ready to
initiate the operation. In an execution where load imbalance or
system noise desynchronize processes, this may cause a performance
penalty resulting from reducing the opportunity of
communication/computation overlap.
%Furthermore, we replace MPI\_Waitany with shmem\_fence (Line 8 in Listing~\ref{listing3})
%to ensure correct synchronization.

Except for the above communication semantics difference, most of the
communication parameters at the sender remain the same (e.g., sender buffer
address and size, target process). A notable exception is the target buffer
address, or at least the displacement, which must be known at the
sender. Ultimately, this complication stems from the different address space
models between one-sided and two-sided operations.

\lstset{tabsize=2, numbers=left}
\begin{lstlisting}[frame=tb,float,
caption={Determining an appropriate buffer size for all processes (\oshmem version)},captionpos=b,label=listing2] 
...
//calculate local buffer size
remap_temp = nqty*out.isize*out.jsize*out.ksize; 
shmem_int_max_to_all(&scratch_size, &remap_temp, 1, 0, 0, nprocs,
                       pWrk1, pSync1);
plan->scratch = (FFT_DATA *) shmalloc(scratch_size*sizeof(FFT_DATA));
...
\end{lstlisting}

\textbf{Address space difference.} MPI utilizes a separate address
space (i.e., each MPI process has a private address space), while
\oshmem utilizes a partitioned global address space. The main
advantage of the latter model is that it enables truly passive target
operations to progress without the target process providing the
receive buffer, however, that also means the target buffer address
must be pre-established at the sender.

In \shput, the target data address (\verb+plan->scratch+ in our case) must be
globally addressable to be accessible remotely. Hence, the memory space
referenced by \verb+plan->scratch+ is allocated in the partitioned global
address space of \oshmem (line 6 in Listing~\ref{listing2}). Furthermore, the
size of this memory space at each process is determined by the largest memory
space in the original MPI program. The purpose is to ensure that each process
uses a same-sized memory space, in line with the \emph{symmetric heap} memory
model of \oshmem. The symmetric heap requires that a remotely accessible data
object has the same type and size at all processes; this concept simplifies the
management of the global memory space at the expense of increasing memory
consumption when some processes could require less memory space. Another, non
performance critical result, is the need to determine the largest memory space
among all processes, 
% we use a collective operation during initialization
% (\texttt{shmem\_int\_max\_to\_all},
shown line 4 in Listing~\ref{listing2}).

\textbf{Synchronization difference.}
MPI introduces delicate control over the establishment of groups
participating in communication operations. A communicator can be
divided into multiple sub-communicators according to an arbitrary
division. As a result, the synchronization operations can happen
either within the whole world or within a restricted
communication group. Because \oshmem attempts to avoid having to
perform message matching, to reduce the latency, \oshmem, on the
other hand, does not have such fine-grained synchronization
operations. \oshmem only defines global synchronizations (e.g.,
\texttt{shmem\_barrier\_all} or \texttt{shmem\_quiet}) and structured
group based operations (essentially, groups of processes whose
identifier is separated by a constant, power of 2, stride). Using a global
synchronization operation in \oshmem to replace a local
synchronization operation in MPI will not affect program correctness,
but would negatively impact performance.

\lstset{tabsize=2, numbers=left}
\begin{lstlisting}[frame=tb,float,
caption={Communication code excerpt from \remap in LAMMPS (\oshmem version)},captionpos=b,label=listing3] 
...
for (isend = 0; isend < plan->nsend; isend++) {
  plan->pack(&in[plan->send_offset[isend]],
           plan->sendbuf, &plan->packplan[isend]);
  shmem_double_put(&scratch[plan->remote_offset[plan>send_proc[isend]]],       
           plan->sendbuf, plan->send_size[isend],plan->send_proc[isend]);
}
shmem_fence();
for (isend = 0; isend < plan->nsend; isend++) {
  shmem_int_p(&grecv[myRank], myRank, plan->send_proc[isend]);
} 
...
while( i < plan->nrecv) {
  for (irecv = 0; irecv < plan->nrecv; irecv++){
     if(grecv[plan->recv_proc[irecv]] == (plan->recv_proc[irecv])){
        plan->unpack(&scratch[plan->recv_bufloc[irecv]],
                 &out[plan->recv_offset[irecv]],&plan->unpackplan[irecv]);
        grecv[plan->recv_proc[irecv]] = -9;
        i++;
     }
  }
}
    ...
\end{lstlisting}


Finally, the original MPI program employs \mpiwaitany to complete receives and
logically synchronize processes.  In the \oshmem-based implementation, to avoid
using global synchronization, we explicitly synchronize pairs of processes.  We
set a marker in the memory before any communication happen. These markers are
overwritten by the source processes upon completion of the data transfer, and
after issuing a fence. The target processes snoop these memory locations to
determine the completion of the \texttt{shmem\_put} operation. Once all markers
are overwritten, all communications have completed.

Lines 13-22 in Listing~\ref{listing3} depict the implementation of the above
method. This approach should enjoy a performance advantage over the
implementation based on a global synchronization operation.  Note that \oshmem
has \texttt{shmem\_wait} to synchronize one sender and receiver, however
employing this construct would order the synchronization of operations between
different peers, hence would not exhibit sufficient asynchrony to accommodate
the uneven load balance in our case.

% \begin{comment} 
%   The first challenge for \oshmem is how to let the senders know the buffer
%   addresses of targets. In the original code, the target buffer address is
%   stored on the receiver side but not known to the sender. In the \oshmem
%   version, we create an array sender\_remote\_offset on the sender. The receiver
%   puts the target buffer address to the sender\_remote\_offset[receiver's rank]
%   with a call of shmem\_int\_p. This step is completed when the remap plan is
%   created so that it will only be called once for each plan.
   
%   The second challenge is \oshmem synchronization. The original MPI processes
%   are divided into 8 groups, one remap plan for each group. Each plan works
%   independently while calling the \remap function. For synchronization a simple
%   shmem\_barrier\_all or a shmem\_quiet after shmem\_put won't work. We
%   redesigned the code with a shmem\_put followed by a shmem\_fence and a
%   shmem\_int\_p. The sender's rank, treated as a signal marker, is sent to the
%   receiver via shmem\_int\_p. A while loop is used to spy the receiving of the
%   marker. As far as the receiver receives the marker it knows that the expected
%   data has been ready for the next step of computation. We don't use
%   shmem\_int\_wait\_until because it causes a mandatory order for checking the
%   marker. The redesigned \oshmem code is listed in
%   Fig.~\ref{fig:shmem_remap_3d}.
 
%  \begin{figure}[htp]
%  \center
%  \includegraphics[scale=0.35]{figures/shmem_remap_3d.png}
%  \caption{\oshmem code for \remap function}
%  \label{fig:shmem_remap_3d}
%  \end{figure} 
% \end{comment}

