\section{Evaluation}
\label{sec:evaluation}

% In this section, we evaluate the performance of the \oshmem-based implementation
% versus the original MPI version. The goal is assert the performance of our
% hybrid \oshmem-MPI approach, especially in a large-scale system.

During the evaluation, we perform both strong scaling and weak scaling
experiments by using the \textit{rhodo} protein simulation input problem. For
the strong scaling case, the input problem size is $8\times 8\times 8\times 32$K
atoms. For the weak scaling tests, the input problem size per core is constant
with a base of 32K atoms per core. We perform the evaluation on Titan, a Cray
XK7 supercomputer located at ORNL.
% Titan contains 18,688 physical compute nodes. Each compute node contains
% 16-Core 2.2GHz AMD Opteron processor and 32 GB of RAM.  Two nodes share one
% Gemini high-speed interconnect router.  Titan employs Cray Gemini high-speed
% interconnect.  In addition to the Opteron CPU, all Titan's physical compute
% nodes contain an NVIDIA Kepler accelerator (GPU) with 6GB of DDR5 memory.
We employ Cray-MPICH 6.3.0 and Cray-shmem 6.3.0.
% \begin{comment}
%   The profiling was collected on eos, a Cray XC30 cluster with 744 nodes and
%   total of 47.6TB of memory. The processor on eos is the Intel Xeon
%   5E-2670. Each XC30 CPU has 16 cores per node for a total of 11,904 traditional
%   processor cores. The eos system is considered a support for projects running
%   on Titan. It has similar software environment to Titan. All our test jobs were
%   finished on eos.
% \end{comment}
%
%\subsection{Evaluation Methodology}
%
Even when two different supercomputer allocations span the same number of nodes,
they may be distributed on different physical machines, connected by a very
different network topology. This has been known to cause undesired performance
variability that prevent comparing the performance obtained from different
allocations.
%~\cite{intel:process_variation,micro06:processs_variation}. 
We eliminate this effect by comparing MPI versus \oshmem on the same allocation, on
an average of 5 repetitions per experiment.

% \begin{comment}
%   \textbf{The following part needs to be updated.}  In the hybrid version of MPI
%   and \oshmem, both MPI and \oshmem are initialized at the beginning. To make
%   the comparison more reasonable we don't count the initialization time in all
%   the performance evaluation. Firstly we measure the time of the hybrid code
%   without any \oshmem library call compared with the original MPI. As shown in
%   Fig.~\ref{fig:lammps_init} the time for the two versions are almost equal.
% \end{comment}

% \begin{figure}[htp]
%  \center
%  \includegraphics[width=10cm, height=5cm]{figures/lammps_init.png}
%  \caption{A demonstration of fair performance comparison between MPI and \oshmem}
%  \label{fig:lammps_init}
%  \end{figure} 

%  To demonstrate that we enable a fair performance comparison, we measure
%  LAMMPS performance of MPI and \oshmem without including those modified
%  communication in Section~\ref{sec:method} and without counting initialization
%  time of MPI and \oshmem.  \oshmem performance is identified with ``hybrid''
%  in the figure, because the current \oshmem version of LAMMPS has both \oshmem
%  and MPI.  From those tests, we expect to see their performance is the same.
%  Figure~\ref{fig:lammps_init} shows the results. In general, their performance
%  difference is less than 0.01\%, even if in a large scale with 4096 cores.

%\subsection{Performance Results for \remap}
\subsection{Performance Results}

\begin{figure}[tb]
 \begin{tabular}{p{1ex}p{.49\textwidth}|p{.49\textwidth}}
  ~ & \parbox{\linewidth}{\center Weak Scaling} & \parbox{\linewidth}{\center Strong Scaling}
\\  
  \parbox[t]{\linewidth}{\rotatebox[origin=c]{90}{Without MPI\_Barrier}} & 
  \parbox{\linewidth}{\center\includegraphics[width=\linewidth]{figures/remap3d_weakscal.pdf}} &
  \parbox{\linewidth}{\center\includegraphics[width=\linewidth]{figures/remap3d_strscal.pdf}} 
\\\hline
  \parbox[t]{\linewidth}{\rotatebox[origin=c]{90}{With MPI\_Barrier}} & 
  \parbox{\linewidth}{\center\includegraphics[width=\linewidth]{figures/remap3d_weakscal_Barrier.pdf}} &
  \parbox{\linewidth}{\center\includegraphics[width=\linewidth]{figures/remap3d_strscal_Barrier.pdf}} 
\end{tabular}
\caption{Comparison of \remap execution time between \oshmem and MPI}
\label{fig:remap}
\end{figure} 

Figure~\ref{fig:remap} presents a comparison of the performance
results for the function \remap, the most communication intensive
function, with more than 90\% of the time spent in communication
routines. In this figure, we consider the weak and strong scaling on
the left and right respectively, and the pure cost of the \remap
function without synchronizing between phases, and the cost including
an additional \texttt{MPI\_Barrier} synchronization to prevent the
next iteration from issuing \shput and modify the dataset on which
current iteration is computing.

For the weak scaling test, the performance of \oshmem is slightly, but consistently,
better than that of MPI, if we do not include the cost of
synchronizations. As the system scale becomes larger, the performance
benefit of \oshmem becomes larger, indicating that \oshmem programming
approach has the potential to exhibit better scalability and higher performance
with the increase in scale. This is mainly due to the better overlapping between different
\texttt{shmem\_put}, when the original loop over \texttt{MPI\_Send} was more synchronizing.
%
For the strong scaling tests, the performance of \oshmem exceeds that
of MPI in most cases as long as we do not include the cost of 
additional synchronizations. In the best case (2048 cores), \oshmem exhibits a
28\% performance improvement over MPI (2.22s vs. 3.12s). 
\begin{comment}
However, for
the case of 1024 cores, the performance of \oshmem is worse than that
of MPI. Furthermore, it can be noted that the performance of MPI at
1024 cores is also better than that at 2048 cores, which is
inconsistent with the increasing performance trends as the system
scale increases. We suspect a good MPI performance at 1024 cores
comes from friendly interconnect topology and node-process mapping.
\end{comment}

When including the cost of the additional synchronization, which
is necessary between calls to \remap in order to ensure that all
processes are done reading and ready to write new data, the time to
completion of the \remap function increases and lose its advantage
compared to the MPI implementation; in some cases the execution time
becomes higher with \oshmem than with MPI. This illustrates a
limitation of the approach taken to integrate \oshmem constructs in
this code, which relies on coarse synchronization techniques, as
finer group-level synchronizations available in \oshmem are not
adapted to this communication scheme.

\begin{comment}
  The time for \remap only counts about 9.5 of the total MPI communication as
  mentioned in section 2. To get a better view of the time difference of the
  designed \oshmem code, we first measure the time for the modified \remap
  function itself. Fig.~\ref{fig:remap_ss} presents the strong scalings of both
  \remap\_shmem and \remap\_mpi on 256 to 4096 cores with fixed problem size of
  8*8*8*32k atoms. This figure shows that \remap\_shmem is faster than
  \remap\_mpi in most of the time. With 2048 cores, the time for the original
  \remap\_mpi is 3.12 seconds, while the time for \remap\_shmem is 2.22 seconds,
  which is 28\% reduction of the execution time. It's worth to note that with
  1024 cores \oshmem is slower than MPI with a time increasing of 8\%. MPI at
  1024 cores outperforms 2048 cores by itself. It indicates that the mechanism
  of MPI works better at 1024 cores than 2048 cores. (Q:???How to explain this?
  I ran a second round of measurement. The results are similar. Note: the time
  were averaged from five sets of measurement. Each time MPI and \oshmem was
  executed in the same job shell description, that is on the same nodes.).

Fig.~\ref{fig:remap_ws} is the weak scaling of the two \remap functions . 
\end{comment}

\begin{comment}
%\subsection{Scalability of LAMMPS}

  Our analysis of \remap above shows that the performance of \oshmem is better
  than MPI for a single function. However for the total execution time of LAMMPS
  \oshmem is slightly worse than MPI. Fig.~\ref{fig:lammps_ss} and
  Fig.~\ref{fig:lammps_ws} show the strong scaling and weak scaling of LAMMPS,
  separately. \oshmem presents similar scalability to MPI. The execution of
  \oshmem is about 3\% increasing with 2048 cores and 5\% increasing with 1024
  cores compared with that of MPI. The possible reason is \oshmem introduced
  extra synchronization while doing shmem allocation as demonstrated in section
  3.2.
\end{comment}

Figures~\ref{fig:lammps_ss} and~\ref{fig:lammps_ws} present the
performance of the whole LAMMPS (including synchronization barriers,
in the case of the Hybrid MPI-\oshmem implementation). The
performance difference between both implementations is very small,
with MPI exhibiting slightly better performance in average (from
0.2\% to 5.5\% difference, a range of differences that is close to the
measurement variability, especially in the strong scaling test).

\begin{comment}
Only
the case with performance improvement is the test with 8 cores for the weak
scaling test. This performance loss is unexpected, given the fact that we have
performance benefit in the function \remap.  We suspect that the performance
loss in \oshmem comes from complicated heap memory management between processes
and relative expensive shmem memory allocation.
\end{comment}

\begin{figure}[htp]
 \center
\parbox{.48\linewidth}{
 \center
 \includegraphics[width=.98\linewidth]{figures/lammps_weakscal_Barrier.pdf}
 \caption{Weak scaling for test results for LAMMPS}
 \label{fig:lammps_ws}
}\quad\parbox{.48\linewidth}{
 \center
 \includegraphics[width=.98\linewidth]{figures/lammps_strscal_Barrier.pdf}
 \caption{Strong scaling test results for LAMMPS}
 \label{fig:lammps_ss}
}
 \end{figure} 

\subsection{The Good, the Bad and the Ugly}

This porting effort has lead to a few observations on the entire process. MPI
is considered today as the de-facto programming paradigm for parallel
applications, at the detriment of PGAS type of languages. Shifting from one
programming model to another is not an easy task: it takes time and effort to
educate the developers with the new concepts, and then correctly translate the
application from one programming paradigm to another.

\paragraph*{The Good} Although MPI is a more stable, well-defined API, 
PGAS based approaches have clear benefit for some types of
usage patterns. Until \oshmem reaches the same degree of flexibility and
adaptability, being able to compose the two programming paradigms in the context
of the same application is a clear necessity. Our experiment confirmed that it is
indeed possible to mix the programming models to take advantage 
of the strong features of each one and improve the performance and scalability of a
highly-optimized MPI application.

An area where the PGAS model exhibit a clear advantage over MPI is the
handling of unexpected messages. In MPI applications getting out of sync due to imbalance
in the workload or system noise, most message become unexpected, forcing the MPI library to
buffer some data internally to delay the delivery upon the posting of the corresponding receive 
call. This temporary buffering has an impact on the memory accesses as it
implies additional memory copies and thus extra overhead on the memory bus. As
the memory is always available in OpenSHMEM, even when the target is passive, 
this pathological usage pattern cannot be triggered. In exchange some of the implicit synchronization 
semantic carried by the send-recv model is lost, and explicit synchronization becomes necessary.

% While we were unable to confirm any dramatic performance gain compared with the
% MPI version, the combined MPI plus OpenSHMEM version had been keeping up on the
% scalability side.

\paragraph*{The Bad}

One of the most unsettling missing feature from the OpenSHMEM standard is the
capability to work with irregular process groups. The current group
collective operations are limited to well defined process topologies, mostly
multi-dimensional cubes. As a consequence, they lack the flexibility of 
their MPI counterparts which operate on communicators and can express 
collective behaviors across arbitrary groups of processes. This leads
to less natural synchronization points, especially in non-symmetric cases.

% - Less natural synchronization calls
% - The performance promise is not filling the expectation
% - lack of irregular group operations

\paragraph*{The Ugly}

One of the selling points of the OpenSHMEM is the exposure at the user level of
\emph{bare metal} network primitives, with a promise for a more direct access to the
hardware and higher performance compared with more abstract programming paradigms,
such as MPI. However, the restricted API of OpenSHMEM, and especially the lack
of high level primitives such as collective communication with neighborhood
communication patterns, forces the application developers to design and
implement their own collective operations, making the applications
sensitive to hardware and
network topological feature variations. Thus, the balance between portability and
performance is weaker than in MPI.

% - API very close to the hardware forces a less portable programming approach,
% and forces the user to use a programming model that might be less portable than
% MPI.
