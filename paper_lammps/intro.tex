\section{Introduction}
\label{sec:intro}

\oshmem is an emerging partitioned global address space (PGAS) library interface
specification that provides interfaces for one-sided and collective
communication, synchronization, and atomic operations. The one-sided
communication operations do not require the active participation of the target
process when receiving or exposing data, freeing the target process to work on
other tasks while the data transfer is going on. It also supports some
collective communication patterns such as synchronizations, broadcast,
collection and reduction operations. In addition it provides interfaces for a
variety of atomic operations including both 32-bit and 64-bit
operations. Overall, it provides a rich set of interfaces for implementing
parallel scientific applications.
%
\oshmem implementations are expected to perform well on the hardware composing
modern HPC systems. This expectation stems from the design philosophy of \oshmem
on providing a lightweight and high performing minimalistic set of operations, a
close match between the \oshmem semantic and hardware-supported native
operations provided by high performance interconnects and memory
subsystems. This tight integration between the hardware and the programming
paradigm is expected to result in excelling latency and bandwidth in synthetic
benchmarks.

Despite a rich set of features and a long legacy of native support from vendors,
\oshmem (and its previous variant, SHMEM) have seen a tedious and limited
adoption. The current situation is analogous to a freshly plowed field ready for
seeding, here in the form of an initial effort to design software engineering
practices that enable efficient porting of scientific simulations to this
programming model. This paper explores porting LAMMPS (a production-quality
MPI-based scientific application) to \oshmem. Due to big differences in semantic
and syntax between MPI and \oshmem, there is no straightforward one-to-one
mapping of functionality. In particular, \oshmem features one-sided
communication and partitioned global address space, much unlike most legacy MPI
applications which employ two-sided MPI communication and a private address
space for each MPI process.  Furthermore, MPI provides explicit controls over
communication patterns (e.g., communicator division and communication based on
process grouping and topology) to improve programming productivity, while
\oshmem does not yet have support for these fine grained controls. Hence,
transforming an MPI-based program into a \oshmem-based one remains a difficult
and largely unexplored exercise.

This work is aimed at understanding the opportunities and challenges of using
the \oshmem programming paradigm to translate and design highly scalable
scientific simulations. We reveal the strength and limitation of \oshmem, and
demonstrate performance similar to MPI in a large-scale performance evaluation
(up to 4096 cores on Oak Ridge's Titan supercomputer).
